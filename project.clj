(defproject memorystone "1.0.1-SNAPSHOT"
  :description "FIXME: write description"
  :dependencies [
                     [basicsensei/clj-minecraft "1.0.1-SNAPSHOT"]
                     [org.clojure/clojure "1.4.0"]
                     [org.clojure/tools.logging "0.2.3"]
                 ;[org.bukkit/bukkit "1.3.2-R0.1"]
                 [org.bukkit/bukkit "1.1-R5-SNAPSHOT"]

                     ]
  
  :repl-options [:init nil :caught clj-stacktrace.repl/pst+]
  :repositories {
                 ;"spout-repo-snap" "http://repo.getspout.org/content/repositories/snapshots/"
                 ;"spout-repo-rel" "http://repo.getspout.org/content/repositories/releases/"
                 "org.bukkit" "Bukkit" "bukkit" {:url "http://repo.bukkit.org/content/groups/public/"}
                 })
